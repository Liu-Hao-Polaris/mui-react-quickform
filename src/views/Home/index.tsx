import { useState } from 'react'
import FieldSetLeft from '@/components/FieldSetLeft'
import FieldSetCenter from '@/components/FieldSetCenter'

import './index.less'

export default () => {
  const [currentField, setCurrentField] = useState({})
  const [fieldList, setFieldList] = useState([])

  return (
    <>
      <div className="fieldSet-main">
        <div className="fieldSet-main-container">
          <div className="fieldSet-main-container-left">
            <FieldSetLeft />
          </div>
          <div className="fieldSet-main-container-center">
            <FieldSetCenter
              fieldList={fieldList}
              currentField={currentField}
              onFieldChange={(list) => setFieldList(list)}
              handleChooseField={(field) => setCurrentField(field)}
            />
          </div>
          <div className="fieldSet-main-container-right"></div>
        </div>
      </div>
    </>
  )
}
