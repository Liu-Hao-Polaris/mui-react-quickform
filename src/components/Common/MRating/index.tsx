import Rating from '@mui/material/Rating'

interface Props {
  max: number
}
export default function CustomizedRating(props: Props) {
  const { max = 10 } = props
  return (
    <>
      <Rating name="customized-10" defaultValue={2} max={max} />
    </>
  )
}
