import Accordion from '@mui/material/Accordion'
import ArrowForwardIosSharpIcon from '@mui/icons-material/ArrowForwardIosSharp'
import { AccordionSummary } from '@mui/material'
import AccordionDetails from '@mui/material/AccordionDetails'
import Typography from '@mui/material/Typography'
import './index.less'

interface Props {
  title: string
  children?: React.ReactNode
}
export default (props: Props) => {
  const { title, children } = props
  return (
    <Accordion disableGutters elevation={0} square>
      <AccordionSummary
        expandIcon={<ArrowForwardIosSharpIcon sx={{ fontSize: '0.9rem' }} />}
      >
        <Typography>{title}</Typography>
      </AccordionSummary>
      <AccordionDetails>{children}</AccordionDetails>
    </Accordion>
  )
}
