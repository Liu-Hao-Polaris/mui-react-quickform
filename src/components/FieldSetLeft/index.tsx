import { ReactSortable } from 'react-sortablejs'
import { nanoid } from 'nanoid'

import './index.less'
interface FieldType {
  id: string | number
  fieldKey: string
  fieldLabel: string
  fieldConfig: object
}

const fieldList: FieldType[] = [
  {
    id: nanoid(),
    fieldKey: 'TextField',
    fieldLabel: '单行文本框',
    fieldConfig: {},
  },
  {
    id: nanoid(),
    fieldKey: 'Button',
    fieldLabel: '按钮',
    fieldConfig: {},
  },
  {
    id: nanoid(),
    fieldKey: 'Rating',
    fieldLabel: '单选框',
    fieldConfig: {},
  },
]

export default () => {
  return (
    <>
      <div className="title">基础组件</div>
      <ReactSortable
        list={fieldList}
        animation={200}
        group={{
          name: 'sort-field',
          pull: 'clone',
          put: false,
        }}
        setList={() => {}}
        sort={false}
        style={{ overflow: 'auto' }}
        forceFallback={true}
      >
        {fieldList.map((item) => (
          <div className="sort-item" key={item.id}>
            {item.fieldLabel}
          </div>
        ))}
      </ReactSortable>
    </>
  )
}
