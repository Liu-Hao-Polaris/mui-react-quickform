import { TextField, Button, Rating } from '@mui/material'
import MButton from '../Common/MButton'
import { useMemo } from 'react'

const FIELD_MAP = {
  TextField,
  Rating,
  Button: MButton,
}

export default ({ field = {} }) => {
  const Component = useMemo(() => {
    return FIELD_MAP[field?.fieldKey]
  }, [field?.fieldKey])

  return <Component />
}
